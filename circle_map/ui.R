library(shinydashboard)
library(shiny)
library(leaflet)

header <- dashboardHeader(title='Census 2011')

sidebar <- dashboardSidebar(
  tags$h3(style="margin-left: 10px;","Commuting in Belgium"),
  
  tags$head(tags$style("#commuter_map{height:85vh !important;}")),
  
  radioButtons("direction", "Select direction", 
               choices = c("Inbound", "Outbound"), selected="Inbound"),
  
  selectInput("municipality", "Choose a municipality", choices = c(11001)),
  
  tags$p(style="margin: 15px; margin-top:30px;", "This aplication ", 
         tags$a(href="https://github.com/maarten-vermeyen/census-2011-commuters", "(source code)"), 
         "offers a visualisation of the number of inbound and outbound commuters in 
         Belgium. It's based on", 
         tags$a(href="http://statbel.fgov.be/nl/statistieken/opendata/datasets/census2011/big/tf_census_2011_commuters_munty_l.jsp", 
         "Open Data on commuting in Belgium"), "as part of the Cencus 2011 dataset. 
         This dataset was released as open data by the FOD financiën on the 4th 
         of february 2016.")
)

body <-dashboardBody(
            fluidRow(
              box(leafletOutput("commuter_map"), width=12)
            )
)

dashboardPage(header, sidebar, body)

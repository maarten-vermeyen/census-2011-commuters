---
title: "Antwerp comuting"
author: "Maarten Vermeyen"
date: "February 21, 2016"
runtime: shiny
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

```

## Commuting in Antwerp

We use the recently released census 2011 dataset on commuting to visualise the outbound
commuting traffic fron The city of Antwerp.

The following libraries are used in making the visualisation:

```{r libraries}
suppressPackageStartupMessages(library('dplyr'))
suppressPackageStartupMessages(library('leaflet'))
suppressPackageStartupMessages(library('rgdal'))
```

## Data loading and preperation

We combine two datasets in order to create the map:

 * [Open data on commuting in Belgium](http://statbel.fgov.be/nl/statistieken/opendata/datasets/census2011/big/tf_census_2011_commuters_munty_l.jsp)
   as part of the Cencus 2011 dataset. This dataset
   was released as open data by the FOD financiën on the 4th of februari 2016.
 * The [boundries of the Flemish municipalities](http://www.geopunt.be/catalogus/datasetfolder/670dc426-370a-4edc-ac65-6c4bcc065773),
   as maintained by AGIV and released under an open data licence.

``` {r load_data}
if (file.exists("data/cached_data.rds")) {
  municipalities <- readRDS("data/cached_data.rds")
} else {
  d <- read.csv('data//TU_CENSUS_2011_COMMUTERS_MUNTY.txt', sep = '|', header = TRUE)
  d <- select(d, CD_MUNTY_REFNIS_RESIDENCE, 
               TX_MUNTY_RESIDENCE_DESCR_NL, 
               CD_MUNTY_REFNIS_WORK, 
               TX_MUNTY_WORK_DESCR_NL,
               OBS_VALUE)

  # Ignore unknown travel destinations and commuters within the city of Antwerp
  d <- subset(d, !(CD_MUNTY_REFNIS_WORK %in% c(" ",'-', "11002")))

  # Limit the data to residents of the city of Antwerp
  d <- subset(d, CD_MUNTY_REFNIS_RESIDENCE == 11002)

  # load municipalitiesdata from AGIV downloaded previously 
  municipalities <- readOGR("data/Refgem.shp",
                             layer = "Refgem", verbose = FALSE)
  # reproject data
  municipalities <-  spTransform(municipalities, CRS = CRS("+init=epsg:4326"))

  # merge geographical data with commuter data
  municipalities <- sp::merge(municipalities, d, by.x = "NISCODE", by.y = "CD_MUNTY_REFNIS_WORK", all.x = TRUE)

  # Set NA to 0
  municipalities$OBS_VALUE[is.na(municipalities$OBS_VALUE)] <- 0
  saveRDS(municipalities, "data/cached_data.rds" )
}
```

## Creating the map

The map is build with leaflet. In order to provide more context to the map, I've added the [GRB Web Mapping Service](http://www.geopunt.be/catalogus/webservicefolder/bb99337d-2146-413e-8c68-b9930061dc25).
This layer is provided by AGIV and is also available under an open data licence.

```{r map}
# Create a continuous palette function
pal <- colorNumeric(
  palette = "Blues",
  domain = municipalities$OBS_VALUE
)

# Create map
m <- leaflet()
m <- setView(m, 4, 51.2, zoom = 8)

m <- addPolygons(m, data = municipalities,
                 stroke = TRUE, opacity = 1, weight=1, fillOpacity = 0.5, smoothFactor = 0.5,
                 color="#3F3F3F", fillColor = ~pal(OBS_VALUE), group= "Commuters from Antwerp")

m <- addWMSTiles(m,
                 baseUrl = "http://grb.agiv.be/geodiensten/raadpleegdiensten/GRB-basiskaart/wmsgr",
                 layers = "GRB_BSK",
                 options = WMSTileOptions(format = "image/png", transparent = TRUE),
                 attribution = 'AGIV',
                 group='grb'
)

m <- addLayersControl(m,
                      baseGroups = c("grb"),
                      overlayGroups = c("Commuters from Antwerp"),
                      options = layersControlOptions(collapsed = FALSE)
)
m
```
